package org.example;

public class ArithmeticOperation {

    //метод вычитания
    public static double substract (double a, double b) {

        return a-b;
    }

    //метод сложения
    public static double sum (double a, double b) {

        return a+b;
    }

    //метод деления
    public static double division (double a, double b){

        return a/b;
   };

    //метод умножения
    public static double multiplication (double a, double b){

        return a*b;
    };
}
