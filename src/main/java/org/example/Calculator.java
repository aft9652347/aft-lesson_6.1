package org.example;

import java.util.InputMismatchException;
import java.util.Scanner;


public class Calculator {
    public static void main(String[] args) {

        //Конструктор для использования сканер
        Scanner scanner = new Scanner(System.in);

//        Просим пользователя ввести выражение
//        System.out.println("укажи какое арифметическое действие нужно выполнить");
//        String arithmeticOperation = scanner.nextLine();
        //переменные для сохранения аргументов
        double a = 0; // первый аргумент
        double b = 0; // второй аргумент
        String arithmeticOperation = null; // арифметический оператор
        double result = 0; // результат

        //Запускаем цикл, чтобы не перезапускать программу
        //while (true) {
                //Блок кода в котором могут возникнуть исключения
            try {
                //Запросить первый аргумент
                System.out.println("Введите первое число:");
                a = scanner.nextDouble();
                //Запросить арифметический оператор
                System.out.println("Укажи какое арифметическое действие нужно выполнить");
                arithmeticOperation = scanner.next();
                //Запросить второй аргумент
                System.out.println("Введите второе число:");
                b = scanner.nextDouble();
                //Блок обработки исключений
            } catch (InputMismatchException incorrectData) {
                System.out.println("Ты чё, введи нормальное число! Если дробь, то через запятую");//обработка ошибки
                incorrectData.printStackTrace();
                throw incorrectData;
            } //finally { в данном случае блок finally не нужен
                //Условия для обработки арифметических операторов
                switch (arithmeticOperation) {
                    case ("+"):
                    case ("плюс"):
                        //вызвать метод сложения
                        result = ArithmeticOperation.sum(a, b);
                        break;
                    case ("-"):
                    case ("минус"):
                        result = ArithmeticOperation.substract(a, b);
                        break;
                    case ("*"):
                    case ("умножить"):
                        result = ArithmeticOperation.multiplication(a, b);
                        //result=a*b;
                        break;
                    case ("/"):
                    case ("разделить"):
                        if (b == 0) {
                            throw new ArithmeticException("Ты чё, на ноль делить нельзя");
                            //ArithmeticException divisionByZero = new ArithmeticException("Ты чё, на ноль делить нельзя");
                            //divisionByZero.printStackTrace();
                            //throw divisionByZero;
                        } else {
                            result = ArithmeticOperation.division(a, b);
                        }
                        break;
                    // если выше перечисленные условия не подошли, то по умолчанию дубет выводится сообщение о некорректном вводе
                    default:
                        System.out.println("Введёно неверное арифметическое выражение, попробуйте ещё раз");
                        ArithmeticException incorrectArithmeticOperation = new ArithmeticException();
                        throw incorrectArithmeticOperation;
                }
                //Условия для обработки итогового результату, если число не инт то переводить его в инт,
                // в противном случае выводит с дробной частью до сотых
                if (result == (int) result) {
                    System.out.println(String.format("%.0f %s %.0f = %d", a, arithmeticOperation, b, (int) result));
                } else {
                    // System.out.println(String.format("%f %s %f = %f", a, arithmeticOperation, b, result));
                    System.out.println(String.format("%.2f %s %.2f = %.2f", a, arithmeticOperation, b, result));
                }
        //}
    }
}